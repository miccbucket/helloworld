package it.unifi.micc.java_base;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HelloWorld {

	public static void main(String[] args) {
		User u=new User(args[0]);
		System.out.println("Hello " + u.getName() + "!");
		boolean empty_string=false;
		while (!empty_string){
			System.out.println("Is this your name? Please enter the correct one: (insert empty string to exit)");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			String name=null;
			try{
				name = br.readLine();
			}catch(Exception e){
				System.out.println(e);
			}
			if (name.length()>0){
				u.setName(name);
				System.out.println("Hello " + u + "!");
			} else {
				empty_string=true;
				System.out.println("Fine di HelloWorld");
			}
		}

	}

}
